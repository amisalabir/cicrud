<?php

class PrivilegedUser extends Role
{
	private $roles;
	public function __construct()
	{
		parent::__construct();
		$this->roles= array();
		//$this->load->model('Role');
	}

	//override  User method
	public function getByUsername($username){

		$sql = "SELECT * FROM users WHERE username = '$username'";
		$STH = $this->db->query($sql);

		$query=$STH->result();
		$result = json_decode(json_encode($query[0]),true);

		//var_dump($result);
	
		if (!empty($result)) {
			$privUser = new PrivilegedUser();

			$privUser->user_id = $result["user_id"];
			$privUser->username = $username;
			$privUser->password = $result["user_password"];
			$privUser->email_addr = $result["user_email"];
			$privUser->initRoles();
			return $privUser;

		}else{
			return false;
		}
	}

	//populate roles with their associated permissions
	public function initRoles(){
		$this->roles = array();
		$sql = "SELECT t1.role_id, t2.role_name FROM user_role as t1 JOIN roles as t2 ON t1.role_id = t2.role_id WHERE t1.user_id = '$this->user_id'";
		$STH = $this->db->query($sql);
		//$sth->execute(array(":user_id"=> $this->user_id));
		$row = $STH->result();	
		//var_dump($row);
		
        foreach ($row as $value) {
          //echo $value->role_name;
        	
          $this->roles[$value->role_name] = Role::getRolePerms($value->role_id);
        }
        	//var_dump($this->roles);
		
	}

	public function hasPrivilege($perm){

		foreach ($this->roles as $role) {
			if ($role->hasperm ($perm)){

				return true;
			}
		}
		return false;
	}

	public function hasRole($role_name){
		return isset($this->roles[$role_name]);
	}

	public static function insertPerm($role_id, $perm_id){
		$sql = "INSERT INTO role_perm (role_id, perm_id) VALUES (:role_id,:perm_id)";
		$sth = $GLOBALS["DB"]->prepare($sql);
		$sth->execute(array(":role_id"=> $role_id, ":perm_id"=>$perm_id));
	}

	public static function deleteperms(){
		$sql = "TRUNCATE role_perm";
		$sth = $GLOBALS["DB"]->prepare($sql);
	return $sth->execute();
	}
}