<?php
class Eimsmodel extends CI_Model{

    public function get_statement($mtype){
        $sql='';
        if($mtype){
            $this->from_Transaction = date('Y-m-01'); // hard-coded '01' for first day
            $this->to_Transaction  = date('Y-m-t');
            $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, student.name as studentname,  X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.paidto, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM tblentries ) X JOIN ( SELECT *, amountIn-amountOut bal FROM tblentries ) Y ON Y.id <= X.id LEFT JOIN  student ON X.product_id = student.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'  AND X.transactionType <> 'OB'  GROUP BY X.id ORDER BY X.transactionDate, X.voucherNo ASC,X.id;";
        }else{
            //$this->from_Transaction = $_GET['fromTransaction']; // hard-coded '01' for first day
            //$this->to_Transaction  = $_GET['toTransaction'];
            $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, student.name as studentname,  X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.paidto, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM tblentries ) X JOIN ( SELECT *, amountIn-amountOut bal FROM tblentries ) Y ON Y.id <= X.id LEFT JOIN  student ON X.product_id = student.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.branchid='$this->branch_id' AND X.transactionType <> 'OB'  GROUP BY X.id ORDER BY X.transactionDate, X.voucherNo ASC,X.id;";
        }

        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getView(){

        $sql='';
        if($_GET['bookname']=='ALLSTUDENT'){
            $sql = "SELECT student.id,student.name ,student.section, student.mobile,student.admissiondate,class.name as classname,batch.year, course.name as coursename   FROM student LEFT JOIN class on student.classid=class.id LEFT  JOIN  batch on student.batchid=batch.id LEFT  JOIN course ON student.courseid=course.id WHERE student.soft_deleted='No'";
        }
        if($_GET['bookname']=='viewhead'){
            $sql = "SELECT * FROM accounthead WHERE soft_deleted='No' order by headnameenglish ASC";
        }

        $query = $this->db->query($sql);
        return $query->result();
    }



    public function insert_trasnsaction_record(){
        //var_dump($_POST);
        $data = array(
            'branchid' => $this->input->post('branchid'),
            'customerId' => $this->input->post('customerId'),
            'transactionType' => $this->input->post('transactionType'),
            'transactionDate' => $this->input->post('transactionDate'),
            'product_id' => $this->input->post('product_id'),
            'shipexheadid' => $this->input->post('shipexheadid'),
            'particulars' => $this->input->post('particulars'),
            'accountheadid' => $this->input->post('accountheadid'),
            'bankId' => $this->input->post('bankId'),
            'voucherNo' => $this->input->post('voucherNo'),
            'transactionFor' => $this->input->post('transactionFor'),
            'transactionMode' => $this->input->post('transactionMode'),
            'paidto' => $this->input->post('paidto'),
            'chequeNo' => $this->input->post('chequeNo'),
            'chequeDate' => $this->input->post('chequeDate'),
            'remarks' => $this->input->post('remarks'),
            'inWords' => $this->input->post('inWords'),
            'created' => $this->input->post('created')
        );
        if ($_POST['transactionType'] == 'MPAY') {
        $data['amountOut']=$this->input->post('amount');
        }
        if ($_POST['transactionType'] == 'MREC') {
            $data['amountIn']=$this->input->post('amount');
            }

        return $this->db->insert('tblentries', $data);

        /*
        $data = array(
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description')
        );
        return $this->db->insert('products', $data);
        */
    }
    public function insertNewStudent(){

        $data = array(

            'admissiondate' => $this->input->post('modifiedDate'),
            'fullname' => $this->input->post('studentName'),
            'name' => $this->input->post('nickName'),
            'father' => $this->input->post('fatherName'),
            'mother' => $this->input->post('motherName'),
            'classid' => $this->input->post('classid'),
            'batchid' => $this->input->post('batchid'),
            'courseid' => $this->input->post('courseid'),
            'dob' => $this->input->post('dob'),
            'mobile' => $this->input->post('mobile'),
            'nid' => $this->input->post('nid'),
            'email' => $this->input->post('email'),
            'note' => $this->input->post('remarks'),

        );

        return $this->db->insert('student', $data);

    }

    public function insert_product(){
        $data = array(
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description')
        );
        return $this->db->insert('products', $data);
    }
    public function update_product($id) {

        $data=array(
            'title' => $this->input->post('title'),
            'description'=> $this->input->post('description')
        );
        if($id==0){
            return $this->db->insert('products',$data);
        }else{
            $this->db->where('id',$id);
            return $this->db->update('products',$data);
        }
    }


    public function getBranch(){
        $sql="select * from branch";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getBook(){
        $sql="select * from book";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getHead(){
        $sql="select * from accounthead";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getClass(){
        $sql="select * from class";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getBatch(){
        $sql="select * from batch";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getCourse(){
        $sql="select * from course";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getdonor(){
        $sql="select * from donor";
        $query = $this->db->query($sql);
        return $query->result();
    }


}

?>