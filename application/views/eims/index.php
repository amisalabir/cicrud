<?php
$this->load->view('includes/header');
$this->load->view('includes/nav');
?>
<div class="content">
		<div class="container ctn">
            <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> </div> </div>"; ?>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
                    <?php /* echo  'statement.php'; if($page=='/transaction.php' || $page=='/inndividualTransaction.php'){echo "inndividualTransaction.php";
}elseif($page=='/orders.php' || $page=='/allorders.php'){echo "allorders.php";} else{echo "statement.php";} */?>
					<form id="searchform" name="searchform" method="GET"   class="signleTranscation" >
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>From</td>
								<td>:</td>
								<td><input type="text" id="datepicker" class="form-control" name="fromTransaction" required></td>
							</tr>
							<tr>
								<td>To</td>
								<td>:</td>
								<td><input type="text" id="todatepicker" class="form-control" name="toTransaction" required></td>
							</tr>
                            <tr>
                                <td>Branch</td>
                                <td>:</td>
                                <td><select  name="branchid" id="branchid" class="form-control" required>
                                        <option value='SELECT'>Select Branch</option>
                                        <?php
                                        foreach ($branches as $branch){
                                            echo "<option value='$branch->id'>$branch->branchname</option>";
                                        }
                                        ?>
                                    </select></td>
                            </tr>
                            <tr id="book">
                                <td>Book</td><td>:</td><td><select  name="bookname" id="bookname" class="form-control text-uppercase" required><option value='SELECT'>Select Book</option>
        <?php foreach ($book  as $singleBook){
            echo"<option value=". $singleBook->value.">". $singleBook->name."</option>";
        } ?>
    </select></td>
                            </tr>
                            <tr id="class" >
                            </tr>
                            <tr id="batch" >
                            </tr>
                            <tr id="head" >
                            </tr>
                            <tr id="sale" >
                            </tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" class="btn btn-primary" name="Search" value="View">
                                    <input type="reset" class="btn btn-primary"  value="Reset">
                                </td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
<?php
	$this->load->view('includes/footer');
	$this->load->view('includes/footer_script');
	if($page=='index'){$this->load->view('includes/footer_script_1');}
	$this->load->view('includes/footer_script_2');
?>