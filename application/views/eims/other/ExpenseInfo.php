<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 main">
					<form class="multipleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Job Expenses</p>
								</div>
							</div>
						</div><hr/>
						
						
						
						<table class=" table table-responsive one">
							<tr>
								<td>
								<div class="col-auto form-inline">
								Category & ID  :
									<input type="text" name="catg" class="form-control" placeholder="Category" required>
									<input type="id" name="jobNo2" placeholder="ID" class="form-control" required>
								</div>
								</td>
							</tr>
						</table>
						<table class="table table-responsive table-bordered tbtwo">
							<thead class="thead-light">
							<tr style="background-color:#4A3C8C;color:#FFFFFF;">
								<th>ID</th>
								<th>Expense ID</th>
								<th>Particulars</th>
								<th>Remarks</th>
								<th></th>
							</tr>
							</thead>
							<tr>
								<td>1</td>
								<td><input type="text" class="form-control" name="ExpId" required></td>
								<td><input type="text" class="form-control" name="Particulars" required></td>
								<td><input type="text" class="form-control" name="remarks" required></td>
								<td style="width:100px;">
									<a href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>&nbsp;
									<a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a>&nbsp;
									<a href="#"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
								</td>
							</tr>
						</table>
						<br/><div align="right"><input type="submit" class="btn btn-primary" name="submit" value="Submit"></div>
					</form>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 