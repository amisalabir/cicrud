<?php
include_once('../../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}
############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
}
################################ End of Session time calculation ##############################

################################ Admin permission ##############################
if($singleUser->role!=='admin') {
    Utility::redirect('index.php');
}

################################ Admin permission ##############################

$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction= new \App\ExpenseIncome\Transaction();
$objImport=new\App\Synchronize\Import();
$objBookTitle->setData($_GET);
$import=$objImport->import();

$msg = Message::getMessage();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

################## object to array ##################
/*Converting Object to an Array*/
$objToArray = json_decode(json_encode($editData), True);
$objToArrayShipex = json_decode(json_encode($shipexhead), True);

if($objToArray['0']['accountheadid']=='24'){
    $objBookTitle->setData($_GET);
    $transactionEditData = $objBookTitle->edit();
    $objToArray = json_decode(json_encode($transactionEditData), True);
}
################## object to array ##################
include_once ('../header.php');
?>
<script type="text/javascript">
    <?php
    //include '../resource/js/edit.js';
    include '../../resource/js/addtransaction.js';
    ?>
</script>

<div class="content">
   <?php if($import){echo "Success";} ?>
</div>
<?php
include ('../footer.php');
include ('../footer_script.php');
if(){
	echo "Success";
}
?>
