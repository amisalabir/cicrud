<?php if($item=='head'){ ?>
<div class="content">
    <div class="container ctn">
        <div class="container"><br></div>
    <form class="form-group" name="HeadEntry" action="store.php" method="post">
        <input hidden name="addHead" type="text" value="addHead">
        <input name="modifiedDate"  type="text" hidden  value="<?php echo date('Y-m-d');?>">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="headnamebangla">HEAD NAME (Bangla) :</label> </div>
                    <div class="col-sm-4 text-left">
                        <input class="form-control " id="" name="headnamebangla" required type="text">
                    </div>
                        <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="headnameenglish">HEAD NAME (English):</label> </div>
                    <div class="col-sm-4 text-left ">
                        <input class="form-control " id="" name="headnameenglish" required type="text">
                        </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="position">POSITION IN TABLE :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <select  name="position"  class="form-control text-uppercase ">
                            <option class='text-uppercase' value='DR'> DEBIT (DR)</option>
                            <option class='text-uppercase' value='CR'> CREDIT (CR)</option>
                            <option class='text-uppercase' value='CONT'> CONTRA (CONT)</option>
                        </select>
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedform">RELATED STATEMENT :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <select  name="relatedform"  class="form-control text-uppercase ">
                            <option class='text-uppercase' value='PL'> PROFIT & LOSS ACCOUNT </option>
                            <option class='text-uppercase' value='TA'> TRADING ACCOUNT </option>
                            <option class='text-uppercase' value='BS'> BALANCE SHEET </option>
                        </select>
                    </div>
                    <div class="col-sm-4"></div>
                </div>

            </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-2 text-right form-group">
                       <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                        <input type="submit" class="btn-primary form-control" value="Submit">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="col-sm-1"></div>
</div>
    </form>
        </div>
    </div>
<?php }
if($item=='student'){
    ?>
    <div class="content">
        <div class="container ctn">
            <div class="container"><br></div>
            <form class="form-group" name="vesselEntry" action="<?php echo base_url('Eims/store'); ?>" method="post">
                <input hidden name="addStudent" type="text" value="addStudent">
                <input name="modifiedDate"  type="text" hidden  value="<?php echo date('Y-m-d');?>">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-4 text-right form-group "><label for="transactionType">STUDENT NAME :</label> </div>
                            <div class="col-sm-4 text-left">
                                <input class="form-control " id="" name="studentName" required type="text">
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group"><label for="wastageValue"> NICKNAME  :</label></div>
                            <div class="col-sm-3 text-left">
                                <input class="form-control" name="nickName"  id="nickName" required type="text">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group "><label for="relatedClient">FATHER'S NAME :</label> </div>
                            <div class="col-sm-4 text-left ">
                                <input class="form-control " id="" name="fatherName"  type="text">
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group"><label for="txtAmount"> MOTHER'S NAME :</label></div>
                            <div class="col-sm-4 text-left">
                                <input class="form-control" name="motherName"  id="motherName"  type="text">
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group "><label for="relatedClient">CLASS :</label> </div>
                            <div class="col-sm-6 text-left ">
                                <select  name="classid"  class="form-control text-uppercase ">
                                    <?php
                                    foreach ($class as $singleClass){
                                        echo  "<option class='text-uppercase' value='$singleClass->id'> $singleClass->name  </option>";}
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group"><label for="lcNo"> BATCH :</label></div>
                            <div class="col-sm-2 text-left">
                                <select  name="batchid"  class="form-control text-uppercase ">
                                    <?php
                                    foreach ($batch as $singleBatch){
                                        echo  "<option class='text-uppercase' value='$singleBatch->id'> $singleBatch->year  </option>";}
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group"><label for="lcNo"> COURSE :</label></div>
                            <div class="col-sm-4 text-left">
                                <select  name="courseid"  class="form-control text-uppercase ">
                                    <?php
                                    foreach ($course as $singleCourse){
                                        echo  "<option class='text-uppercase' value='$singleCourse->id'> $singleCourse->name  </option>";}
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group"><label for="lcDate">  DATE OF BIRTH :</label></div>
                            <div class="col-sm-2 text-left">
                                <input id="datepicker" class="form-control selectDate" name="dob"   type="text">
                            </div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group"><label for="ldt">MOBILE :</label></div>
                            <div class="col-sm-2 text-left">
                                <input class="form-control " id="" name="mobile" required type="text">
                            </div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group"><label for="dollarPrice">NID/BC NO :</label></div>
                            <div class="col-sm-3 text-left">
                                <input class="form-control" name="nid" type="text" >
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 text-right form-group"><label for="dollarRate">EMAIL :</label></div>
                            <div class="col-sm-3 text-left">
                                <input class="form-control" name="email"  type="email" >
                            </div>
                            <div class="col-sm-5"></div>
                        </div>

                        <div class="row">

                            <div class="col-sm-4 text-right form-group"><label for="remarks"> REMARKS :</label></div>
                            <div class="col-sm-5 text-left">
                                <textarea class="form-control"  name="remarks" rows="2" cols="20"  ></textarea>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5"></div>
                        <div class="col-sm-3 text-right form-group">
                            <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                            <input type="submit" class="btn-primary form-control" value="Submit">
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </form>
        </div>
    </div>
    <?php
    } ?>
