<?php
$this->load->view('includes/header');
$this->load->view('includes/nav');
?>
<div class="content">
    <div class="container ctn">
                <div class="container"><br></div>
        <div class="row">
    <form class="form-group" name="transactionEntry" action="<?php echo base_url('Eims/store'); ?>" method="post">
        <input hidden name="addTransaction" type="text" value="addTransaction">
        <input name="modifiedDate" type="text" hidden  value="<?php echo date('Y-m-d');?>">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="transactionType">Transaction Type :</label> </div>
                    <div class="col-sm-4 text-left">
                        <select id="transactionType" name="transactionType" onchange="checkTransactionType(this.value)" style="width:150px; " class="form-control">
                            <option  value="MPAY">PAYMENT</option>
                            <option   value="MREC">RECEIPT</option>
                            <!--
                            <option value="JOUR">JOURNAL</option>
                            <option value="CONT">CONTRA</option>
                            -->
                        </select></div>
                    <div class="col-sm-4"></div>
                </div>
                <!--<div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">Related Client / Party :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <select name="customerId" style="width:150px; " class="form-control text-uppercase ">
                            <option class='text-uppercase' selected value='0'> N/A</option>
                            <?php
                            foreach ($allClients as $individualClient){
                                echo  "<option class='text-uppercase' value='$individualClient->id'> $individualClient->partyname</option>";
                            }
                            ?>
                        </select></div>
                    <div class="col-sm-4"></div>
                </div> -->
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">Branch :</label> </div>
                    <div class="col-sm-6 text-left ">
                        <select  name="branchid" style="width:auto; " class="form-control text-uppercase ">
                            <?php
                            foreach ($branches as $branch){
                                echo "<option value='$branch->id'>$branch->branchname</option>";
                            }
                            ?>
                        </select></div>
                    <div class="col-sm-2"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="accountHead">Account Head :</label> </div>
                    <div id="accountHead" class="col-sm-5 text-left ">
                     <select data-live-search='true'  data-live-search-style='startsWith' class='selectpicker' id="accheadId" name="accheadId" onchange="CheckAccountHeadField(this.value)"   class="form-control text-uppercase ">
                         <option class="text-uppercase">SELECT HEAD</option>
                         <?php
                      foreach ($accountHead as $singleHead){
                      echo "<option  value='$singleHead->id'>$singleHead->headnameenglish ($singleHead->id)</option>";
                      } ?>
                     </select>
                    </div>
                  <div class="col-sm-3"></div>
                </div>
                <div class="row" id="productId">

                </div>
                <div id="weight" style='display:;'>

                </div>
                <div id="activeBankField"   style='display:none;'>
                    <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedBank">Bank Name :</label> </div>
                    <div class="col-sm-5 text-left ">
                        <select id="bankId"  name="bankId"  class="form-control text-uppercase ">
                            <option class="text-uppercase" value="0">SELECT BANK</option>
                      <?php
                            foreach ($bankNme as $singlBank){
                                echo  "<option class='text-uppercase' value='$singlBank->id'> $singlBank->bankname ($singlBank->accountname) </option>";}
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionDate">Transaction Date :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" id="datepicker" name="transactionDate" required placeholder="yyyy-mm-dd" onchange="" onkeypress="" type="text">
                    </div>
                    <div class="col-sm-7"></div>
                </div>
                <div id="voucherNo"  class="row" style="display:block;">
                    <div class="col-sm-4 text-right form-group"><label for="voucherNo"> Voucher No (DR) :</label></div><div class="col-sm-2 text-left"><input class="form-control" value="0" name="voucherNo" type="text" ></div><div class="col-sm-6"></div>
                </div>

                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionFor">  Transaction For :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="transactionFor"   type="text">
                    </div>
                    <div class="col-sm-7"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionMode">Transaction Mode :</label></div>
                    <div class="col-sm-2 text-left">
                        <select id="transactionMode" name="transactionMode" class="form-control" onchange='ChequeField(this.value);' required>
                            <option selected="selected" value="CASH">CASH</option>
                            <option value="CASH CHEQUE">CASH CHEQUE</option>
                            <option value="A/C PAYEE CHEQUE">A/C PAYEE CHEQUE</option>
                            <option value="ONLINE TRANSFER">ONLINE TRANSFER</option>
                            <option value="MOBILE BANK">MOBILE BANK</option>
                            <option value="PAY ORDER">PAY ORDER</option>
                            <option value="ATM">ATM</option>
                            <option value="D.D.">D.D.</option>
                            <option value="T.T.">T.T.</option>
                            <option value="OTHERS">OTHERS</option>
                        </select>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row" id="paidTo"  >
                    <div class="col-sm-4 text-right form-group"><label for="receivedTo">Paid To :</label></div><div class="col-sm-3 text-left"><input class="form-control" name="receivedTo" type="text" ></div><div class="col-sm-5"></div>
                </div>
                <div id="activeChequeField" style='display:;'>

                </div>
                <div class="row">

                    <div class="col-sm-4 text-right form-group"><label for="remarks"> Remarks :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control"  name="remarks" rows="2" cols="20"  ></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="txtAmount"> Amount :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="amount"  id="txtAmount" required type="text">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="inWords">In Words :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control" name="inWords"></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-3 text-right form-group">
                       <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                        <input type="submit" class="btn-primary form-control" value="Submit">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="col-sm-1"></div>
</div>
    </form>

        </div>
    </div>
</div>
<?php
$this->load->view('includes/footer');
$this->load->view('includes/footer_script');
if($page!='index') { $this->load->view('includes/footer_script_1'); }
$this->load->view('includes/footer_script_2');
?>