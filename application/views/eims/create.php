<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}
if($singleUser->role!='admin'){
    Message::getMessage("You are not allowed !");
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################

//$objBookTitle = new \App\MainController\MainController();
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction= new \App\ExpenseIncome\Transaction();
$objClasses=new \App\ExpenseIncome\Classes();
$allData = $objBookTitle->index();
$allClients=$objBookTitle->allClients();
$allparticulars=$objBookTitle->allparticulars();
$accountHead=$objTransaction->accounthead();
$bankNme=$objTransaction->allbank();
$class=$objClasses->classes();
$batch=$objClasses->batch();
$course=$objClasses->course();
$msg = Message::getMessage();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);


include_once ('header.php');
?>
<div class="content">
    <div class="container ctn">
        <div class="row">  <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?> </div>
        <div class="container"><br></div>
        <div class="row">
    <script type="text/javascript">
        function ChequeField(val){
            var element=document.getElementById('activeChequeField');
            if(val=='CASH CHEQUE'){
                element.style.display='block';
            }
            else{
                element.style.display='none';
            }
        }
    </script>

     <?php
     if($_GET['id']=='addStudent'){?>
        <form class="form-group" name="vesselEntry" action="store.php" method="post">
        <input hidden name="addStudent" type="text" value="addStudent">
        <input name="modifiedDate"  type="text" hidden  value="<?php echo date('Y-m-d');?>">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="transactionType">STUDENT NAME :</label> </div>
                    <div class="col-sm-4 text-left">
                        <input class="form-control " id="" name="studentName" required type="text">
                    </div>
                        <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="wastageValue"> NICKNAME  :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="nickName"  id="nickName" required type="text">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">FATHER'S NAME :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <input class="form-control " id="" name="fatherName"  type="text">
                        </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="txtAmount"> MOTHER'S NAME :</label></div>
                    <div class="col-sm-4 text-left">
                        <input class="form-control" name="motherName"  id="motherName"  type="text">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">CLASS :</label> </div>
                    <div class="col-sm-6 text-left ">
                        <select  name="classid"  class="form-control text-uppercase ">
                            <?php
                            foreach ($class as $singleClass){
                                echo  "<option class='text-uppercase' value='$singleClass->id'> $singleClass->name  </option>";}
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="lcNo"> BATCH :</label></div>
                    <div class="col-sm-2 text-left">
                         <select  name="batchid"  class="form-control text-uppercase ">
                            <?php
                            foreach ($batch as $singleBatch){
                                echo  "<option class='text-uppercase' value='$singleBatch->id'> $singleBatch->year  </option>";}
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="lcNo"> COURSE :</label></div>
                    <div class="col-sm-4 text-left">
                        <select  name="courseid"  class="form-control text-uppercase ">
                            <?php
                            foreach ($course as $singleCourse){
                                echo  "<option class='text-uppercase' value='$singleCourse->id'> $singleCourse->name  </option>";}
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="lcDate">  DATE OF BIRTH :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control selectDate" name="dob"   type="text">
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="ldt">MOBILE :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control " id="" name="mobile" required type="text">
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="dollarPrice">NID/BC NO :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="nid" type="text" >
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="dollarRate">EMAIL :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="email"  type="email" >
                    </div>
                    <div class="col-sm-5"></div>
                </div>

                <div class="row">

                    <div class="col-sm-4 text-right form-group"><label for="remarks"> REMARKS :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control"  name="remarks" rows="2" cols="20"  ></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-3 text-right form-group">
                       <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                        <input type="submit" class="btn-primary form-control" value="Submit">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="col-sm-1"></div>
</div>
    </form>
      <?php }
     if($_GET['id']=='addShead'){  ?>
                <form class="form-group" name="HeadEntry" action="store.php" method="post">
             <input hidden name="addHead" type="text" value="addHead">
             <input name="modifiedDate"  type="text" hidden  value="<?php echo date('Y-m-d');?>">
             <div class="row">
                 <div class="col-sm-1"></div>
                 <div class="col-sm-10">
                     <div class="row">
                         <div class="col-sm-4 text-right form-group "><label for="headnamebangla">HEAD NAME (Bangla) :</label> </div>
                         <div class="col-sm-4 text-left">
                             <input class="form-control " id="" name="headnamebangla" required type="text">
                         </div>
                         <div class="col-sm-4"></div>
                     </div>
                     <div class="row">
                         <div class="col-sm-4 text-right form-group "><label for="headnameenglish">HEAD NAME (English):</label> </div>
                         <div class="col-sm-4 text-left ">
                             <input class="form-control " id="" name="headnameenglish" required type="text">
                         </div>
                         <div class="col-sm-4"></div>
                     </div>
                     <div class="row">
                         <div class="col-sm-4 text-right form-group "><label for="position">POSITION IN TABLE :</label> </div>
                         <div class="col-sm-4 text-left ">
                             <select  name="position"  class="form-control text-uppercase ">
                                 <option class='text-uppercase' value='DR'> DEBIT (DR)</option>
                                 <option class='text-uppercase' value='CR'> CREDIT (CR)</option>
                                 <option class='text-uppercase' value='CONT'> CONTRA (CONT)</option>
                             </select>
                         </div>
                         <div class="col-sm-4"></div>
                     </div>
                     <div class="row">
                         <div class="col-sm-4 text-right form-group "><label for="relatedform">RELATED STATEMENT :</label> </div>
                         <div class="col-sm-4 text-left ">
                             <select  name="relatedform"  class="form-control text-uppercase ">
                                 <option class='text-uppercase' value='PL'> PROFIT & LOSS ACCOUNT </option>
                                 <option class='text-uppercase' value='TA'> TRADING ACCOUNT </option>
                                 <option class='text-uppercase' value='BS'> BALANCE SHEET </option>
                             </select>
                         </div>
                         <div class="col-sm-4"></div>
                     </div>

                 </div>
                 <div class="row">
                     <div class="col-sm-5"></div>
                     <div class="col-sm-2 text-right form-group">
                         <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                         <input type="submit" class="btn-primary form-control" value="Submit">
                     </div>
                     <div class="col-sm-5"></div>
                 </div>
                 <div class="col-sm-1"></div>
             </div>
         </form>
     <?php }
     if($_GET['id']=='addBank') { ?>
             <form class="form-group" name="BankEntry" action="store.php" method="post">
             <input hidden name="addBank" type="text" value="addBank">
             <input name="modifiedDate"  type="text" hidden  value="<?php echo date('Y-m-d');?>">
             <div class="row">
                 <div class="col-sm-1"></div>
                 <div class="col-sm-10">
                     <div class="row">
                         <div class="col-sm-4 text-right form-group "><label for="bankname">BANK NAME :</label> </div>
                         <div class="col-sm-4 text-left">
                             <input class="form-control " id="" name="bankname" required type="text">
                         </div>
                         <div class="col-sm-4"></div>
                     </div>
                     <div class="row">
                         <div class="col-sm-4 text-right form-group "><label for="accountname">ACCOUNT NAME:</label> </div>
                         <div class="col-sm-4 text-left ">
                             <input class="form-control " id="" name="accountname" required type="text">
                         </div>
                         <div class="col-sm-4"></div>
                     </div>
                     <div class="row">
                         <div class="col-sm-4 text-right form-group "><label for="accountnumber">ACCOUNT NUMBER :</label> </div>
                         <div class="col-sm-4 text-left ">
                             <input class="form-control " id="" name="accountnumber" required type="text">
                         </div>
                         <div class="col-sm-4"></div>
                     </div>
                     <div class="row">
                         <div class="col-sm-4 text-right form-group "><label for="branch">BRANCH :</label> </div>
                         <div class="col-sm-4 text-left ">
                             <input class="form-control " id="" name="branch" required type="text">
                         </div>
                         <div class="col-sm-4"></div>
                     </div>
                     <div class="row">
                         <div class="col-sm-4 text-right form-group "><label for="address">ADDRESS :</label> </div>
                         <div class="col-sm-4 text-left ">
                             <input class="form-control " id="" name="address" required type="text">
                         </div>
                         <div class="col-sm-4"></div>
                     </div>

                 </div>
                 <div class="row">
                     <div class="col-sm-5"></div>
                     <div class="col-sm-2 text-right form-group">
                         <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                         <input type="submit" class="btn-primary form-control" value="Submit">
                     </div>
                     <div class="col-sm-5"></div>
                 </div>
                 <div class="col-sm-1"></div>
             </div>
         </form>
     <?php }
     if($_GET['id']=='user'){ ?>
           <form action="#" name="addUser" class="signleTranscation" method="post">
             <div class="control">
                 <div class="row">
                     <div class="col-md-3"></div>
                     <div class="col-md-3">
                         <a href="#" class="btn btn-secondary">EDIT</a>
                         <a href="#" class="btn btn-secondary">Refresh</a>
                     </div>
                     <div class="col-md-3">
                         <p class="nick text-right">Create User</p>
                     </div>
                     <div class="col-md-3"></div>
                 </div>
             </div>
               <div class="row">
                   <div class="col-sm-3"> </div>
                   <div class="col-sm-6">
               <table class="table table-responsive" border="0">
                 <tr>
                     <td>Name</td>
                     <td>:</td>
                     <td><input type="text" class="form-control" name="userName" required>
                         <input type="hidden"  class="form-control" name="addUser" value="addUser" required>
                     </td>
                 </tr>
                 <tr>
                     <td>Email</td>
                     <td>:</td>
                     <td><input type="email" class="form-control" name="email" required></td>
                 </tr>
                 <tr>
                     <td>Branch</td>
                     <td>:</td>
                     <td>
                         <select name="branch" class="form-control" required>
                             <option value="Chittagong" selected>Chittagong</option>
                             <option value="Dhaka">Dhaka</option>
                             <option value="Comilla">Comilla</option>
                         </select>
                     </td>
                 </tr>
                 <tr>
                     <td>User Type</td>
                     <td>:</td>
                     <td>
                         <select name="userType" class="form-control" required>
                             <option value="User" selected>User</option>
                             <option value="Admin">Admin</option>
                         </select>
                     </td>
                 </tr>
                 <tr>
                     <td>Password</td>
                     <td>:</td>
                     <td><input type="password" class="form-control" name="password" required></td>
                 </tr>
                 <tr>
                     <td>Confirm Password</td>
                     <td>:</td>
                     <td><input type="password" class="form-control" name="confirmPass" required></td>
                 </tr>
                 <tr>
                     <td></td>
                     <td></td>
                     <td><input type="submit" class="btn btn-primary" name="submit" value="Save"></td>
                 </tr>
             </table>
                  </div>
                   <div class="col-sm-3"> </div>
               </div>
         </form>
     <?php }

     ?>
        </div>
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
