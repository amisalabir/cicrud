<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
//var_dump($_GET); die();
################################ End of Session time calculation ##############################
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction = new \App\ExpenseIncome\Transaction();
$allClients=$objBookTitle->allClients();
$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);
$allData =$objTransaction->setData($_GET);
//var_dump($_GET); die();
    $transactionData = $objTransaction->singleProductSalesledger();
//var_dump($transactionData); die();
/*Branch selection*/
if($_GET['branchid']=='1') $branch="(Head Office)";
if($_GET['branchid']=='2') $branch="(Yard)";
if($_GET['branchid']=='all') $branch="(All Branch)";

/*
if ($singleUser->role=='admin'){
    if ($_GET['salename']!=='all' && ($_GET['branchid']=='1' || $_GET['branchid']=='2' ))
        $transactionData = $objTransaction->singleProductSalesledger();
    if($_GET['salename']!=='all' && $_GET['branchid']=='all')
        $transactionData = $objTransaction->singleBranchallsalesledger();
        if($_GET['salename']=='all' && $_GET['branchid']=='all')
            $transactionData = $objTransaction->allsalesledger();
}else{

    $transactionData = $objTransaction->singleProductSalesledger();
}
*/
//var_dump($transactionData); die();
################## statement  block start ####################

    $_SESSION['someData']=$transactionData;
    //echo "<pre>"; var_dump($allData); echo "</pre>"; die();
    //Converting Object to an Array
    $objToArray = json_decode(json_encode($transactionData), True);
   //echo "<pre>"; var_dump($objToArray); echo "</pre>"; die();
    //$customeName=$objToArray['0']['headnameenglish'];
    $customeName="ALL";

    if($_GET['salename']!=='all'){
        $customeName=$objToArray['0']['product_name'];}


    $sales="SALE";
    //$statementTotal=$objBookTitle->statementTotal();
       $serial = 1;

################## statement  block end ######################
include_once ('header.php');
include_once ('printscript.php');?>
<div align="center" class="content">
    <div class="container ctn">
 <div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<!-- required for search, block 4 of 5 start -->
<!-- Search nav start-->
<?php // include_once ('searchnav.php'); ?>
<!-- Search nav ended-->
<!-- Date selection ended -->
    <form action="trashmultiple.php" method="post" id="multiple">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="navbar-header">
                        <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                    $userButton= "<div id=\"navbarTwo\" class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav navbar-right\">
                            <button type=\"button\"   id=\"btnPrint\" value=\"Print Div Contents\" class=\"btn btn-primary active \">Print</button>
                            <a href=\"pdf.php\" class=\"btn btn-primary \"  >Download as PDF</a>
                            <a href=\"xl.php\" class=\"btn btn-primary\" \" >Download as XL</a>
                            <a href=\"email.php?list=1\" class=\"btn btn-primary\" role=\"button\" >Email</a>
                            ";
                    $adminButton="<button type=\"button\" class=\"btn btn-danger\" id=\"delete\">Delete  Selected</button>
                            <button type=\"submit\" class=\"btn btn-warning\">Trash Selected</button>
                            <a href=\"trashed . php ? Page = 1\"   class=\"btn btn-info\" > View Trashed List</a>
                            </ul></div>
                        ";
                    if($singleUser->role=='admin'){
                        echo $userButton.$adminButton;
                    } else{echo $userButton;}
                    ?>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <!-- <h1 style="text-align: center" ;">Book Title - Active List (<?php //echo count($allData) ?>)</h1>-->
        <span><br><br> </span>
        <div class="container">
            <div id="dvContainer">
                <style>
                    <?php
                    include ('../resource/css/printsetup.css')
                    ?>
                </style>
                <table >
                    <thead>
                    <tr>
                        <td colspan="3" align="center" >
                            <font  style="text-align: center;  text-transform:uppercase; font-weight: bold; font-size:25px;">KAZI SALA UDDIN.</font> <br>
                            <font style="font-size:14px">Colonnelhat, Akbarshah, Chittagong.</font><br>
                            <font style="font-size:13px">(<?php echo "Statement Since : ".$_GET['fromTransaction']." to ".$_GET['toTransaction'];?>)</font>
                        </td>
                    </tr>
                    <tr><td ><b><?php  echo "HEAD: SALE (".$customeName.")"; ?></b></td> <td></td> <td style="text-align: right; font-size: 12;"><?php echo "Print Date: ";  echo date('Y-m-d'); ?> </td></tr>
                    </thead>
                    <tr> <td colspan="3">
                            <!-- Inner Table -->
                            <div class="row" align="center">
                                <div id="reporttable" class="col-sm-12 text-center" align="center" >
                      <table width="100%"   class=""  >
                        </tr>
                          <thead>
                             <tr style="background-color:#F2F2F2;">
                            <th class="text-center"><input id="select_all" type="checkbox" value="select all"></th>
                            <th class="text-center">SL</th>
                            <th class="text-center">Date</th>
                           <th class="text-center" width="350px">Description</th>

                            <th class="text-center">DR<br> Payment <br> (Taka)</th>
                           <th class="text-center">CR<br>Received <br> (Taka)</th>
                            <th class="text-center"> Balance<br> (Taka)</th>
                        </tr>
                          </thead>
                        <?php
                            $totalAmountIn=0;
                            $totalAmountOut=0;
                            $balance=0;
                            $totalWeight=0;

                        //  $serial= 1;  ##### We need to remove this line to work pagination correctly.
                       // echo "<pre>"; var_dump($allData); echo "</pre>"; die();

                        foreach($transactionData as $oneData){
                            ########### Traversing $someData is Required for pagination  #############
                            //$totalDays=abs('$oneData->toDurationDate','$oneData->fromDurationDate');
                            if($serial%2) $bgColor = "AZURE";
                            else $bgColor = "#ffffff";
                            $totalAmountIn=$totalAmountIn+$oneData->amountin;
                            $totalAmountOut=$totalAmountOut+$oneData->amountout;
                            //$totalAmount=$totalAmount+$oneData->amount ;
                            $balance=($balance-$oneData->amountout)+$oneData->amountin;

                            echo "
                  <tr  style='background-color: $bgColor'>
                     <td style='padding-left:5px;' class='text-center'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                     <td style='text-align: center;'>$serial</td>
                      <td class='text-center'>$oneData->transactiondate</td>
                     <td class='text-left'> [$oneData->m]- $oneData->name $oneData->remarks </td>
                     <td style='text-align: right;'>".number_format($oneData->amountout,0)."</td>
                     <td style='text-align: right;'> ".number_format($oneData->amountin,0)."</td>
                     <td class='text-right'></td>

                  </tr>
              ";
                $serial++; }
                $term=""; $ledgerBalance=0;
                if($totalAmountIn==0) $ledgerBalance=$totalAmountOut; $term="DR"; if ($totalAmountIn!==0) $term="CR"; $ledgerBalance=$totalAmountIn;
                  echo "     
                        <tr style='background-color:; text-align:right; height: 50px; font-size: large; font-weight: bold;'>
                            <td class='text-right' colspan='4'> Total:</td>
                            <td class='text-right'>".number_format($totalAmountOut,0)."</td>
                            <td class='text-right'>".number_format($totalAmountIn,0)."</td>
                            <td>$term ".number_format($ledgerBalance,0)."</td>
                        </tr>
                        "; ?>
                    </table>
                    <br>
                </div>

            </div>
            </div>
        </td>
        </tr>
        </table>
    </div>
</div>
  </form>
    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
    <!--  ######################## pagination code block#2 of 2 end ###################################### -->
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
