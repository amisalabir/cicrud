
<div align="center" class="content">
    <div align="center" class="container ctn">
        <form action="trashmultiple.php" method="post" id="multiple">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="navbar-header">
                        <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                    $userButton= "<div id=\"navbarTwo\" class=\"navbar-collapse collapse\">
                        <ul class=\"nav navbar-nav navbar-right\">
                            <button type=\"button\"   id=\"btnPrint\" value=\"Print Div Contents\" class=\"btn btn-primary active \">Print</button>
                            <a href=\"pdf.php\" class=\"btn btn-primary \"  >Download as PDF</a>
                            <a href=\"xl.php\" class=\"btn btn-primary\" \" >Download as XL</a>
                            <a href=\"email.php?list=1\" class=\"btn btn-primary\" role=\"button\" >Email</a>
                            ";
                    $adminButton="<button type=\"button\" class=\"btn btn-danger\" id=\"delete\">Delete  Selected</button>
                            <button type=\"submit\" class=\"btn btn-warning\">Trash Selected</button>
                            <a href=\"trashed . php ? Page = 1\"   class=\"btn btn-info\" > View Trashed List</a>
                            </ul></div>
                        ";

                        echo $userButton.$adminButton;

                    ?>
                </div>
                <div class="col-md-1"></div>
            </div>
            <!-- <h1 style="text-align: center" ;">Book Title - Active List (<?php //echo count($allData) ?>)</h1>-->
            <span><br><br> </span>
            <div class="row">
                <div class=" col-md-3 col-md-offset-8 text-right">
                    <input id="myInput" type="text" placeholder="Search..." >
                </div>
                <div class="col-md-1"></div>
            </div>
            <div class="row"  id="dvContainer" align="center">
                   <style>
                            <?php
                           // echo base_url();
                            include ('../assets/css/printsetup.css')
                            ?>
                        </style>
                         <div class="col-sm-1"></div>
                         <div id="innerTable" class="col-sm-10 text-center" align="center" >
                             <font  style="text-align: center;  text-transform:uppercase; font-weight: bold; font-size:25px;">KAZI SALA UDDIN.</font> <br>
                             <font style="font-size:14px">Colonnelhat, Akbarshah, Chittagong.</font><br>
                             <font style="font-size:13px">(<?php echo "Statement Since :  to ";?>)</font><br>
                             <div><b> <?php  echo "HEAD: Cash Statement "; ?></b> </div> <div ><?php echo "Print Date: ";  echo date('Y-m-d'); ?><div>
                             <table width="auto" class="table table-responsive" id="example">
                                            <thead>
                                            <tr class="class="header"" style="background-color:#F2F2F2;">
                                                <th class="text-center"><input id="select_all" type="checkbox" value="select all"></th>
                                                <th class="text-center">SL</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center" width="500px">Description</th>
                                                <th class="text-center">Voucher/ <br>Challan No</th>
                                                <th class="text-center">Received <br> (Taka)</th>
                                                <th class="text-center">Payment <br> (Taka)</th>
                                                <th class="text-center">Balance</th>
                                            </tr>
                                            </thead>
                                            <tbody id="myTable">
                                            <?php
                                            $serial=1;
                                            $totalAmountIn=0;
                                            $totalAmountOut=0;
                                            $balance=0;
                                            //  $serial= 1;  ##### We need to remove this line to work pagination correctly.
                                            // echo "<pre>"; var_dump($allData); echo "</pre>"; die();
                                            foreach($data as $oneData){ ########### Traversing $someData is Required for pagination  #############
                                                //$totalDays=abs('$oneData->toDurationDate','$oneData->fromDurationDate');
                                                if($serial%2) $bgColor = "AZURE";
                                                else $bgColor = "#ffffff";
                                                $totalAmountIn=$totalAmountIn+$oneData->amountIn;
                                                $totalAmountOut=$totalAmountOut+$oneData->amountOut;
                                                //$totalAmount=$totalAmount+$oneData->amount ;
                                                $balance=($balance-$oneData->amountOut)+$oneData->amountIn;
                                                $voucherType=""; $voucherOrChallan="";
                                                if($oneData->voucherNo!=Null){$voucherType="Dr -"; $voucherOrChallan=$oneData->voucherNo;}else{$voucherType="Cr -"; $voucherOrChallan=$oneData->crvoucher;}
                                                if($oneData->challanno!=Null ||($oneData->voucherNo==Null && $oneData->crvoucher==Null )){$voucherType="Ch -"; $voucherOrChallan=$oneData->challanno;}
                                                echo "
                  <tr  style='background-color: $bgColor'>
                     <td style='padding-left:5px;' class='text-center'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                     <td style='text-align: center;'><a role='menuitem' tabindex=-1' href='edit.php?id=$oneData->id'>$serial</a></td>
                      <td class='text-center'>$oneData->transactionDate</td>
                     <td class='text-left'> $oneData->headnameenglish: $oneData->accountname $oneData->studentname <br> $oneData->transactionFor $oneData->remarks  </td>
                     <td class='text-center'>$voucherType $voucherOrChallan</td>
                     <td style='text-align: right;'>".number_format($oneData->amountIn,0)."</td>
                     <td style='text-align: right;'>".number_format($oneData->amountOut,0)."</td>
                     <td class='text-right'>".number_format($balance,0)."</td>
                  </tr>    </tbody>
              ";
                                                $serial++; }
                                            echo "     
                        <tr style='background-color:; font-weight: bold;'>
                            <td style='text-align: right;' colspan='5'> Total Taka: <span>&nbsp;&nbsp; </span></td>
                            <td class='text-right'>".number_format($totalAmountIn,0)." </td>
                            <td class='text-right'>".number_format($totalAmountOut,0)."</td>
                            <td class='tablefooter text-right'>".number_format($balance,0)." </td>
                        </tr>
                        "; ?>

                      </table>
                      <!--
                      <br><br/><hr/>

                        <table id="example" class="table table-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tiger Nixon</td>
                                <td>System Architect</td>
                                <td>Edinburgh</td>
                                <td>61</td>
                                <td>2011/04/25</td>
                                <td>$320,800</td>
                            </tr>
                            <tr>
                                <td>Garrett Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td>63</td>
                                <td>2011/07/25</td>
                                <td>$170,750</td>
                            </tr>


                        </tbody>
                    </table>-->









                         <div class="col-sm-1"></div>
                     </div>
                <div  class="row">
                    <div class="col-sm-12 tex-center">
                        <a href="<?php echo base_url('eims'); ?>/statement/monthid=prevtMonth&prevDate"><< Previous</a> |
                        <a href="#">Next >></a>
                    </div>
                </div>
        </form>
       <!--  ######################## pagination code block#2 of 2 end ###################################### -->
    </div>
</div>
