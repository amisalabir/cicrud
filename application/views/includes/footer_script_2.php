
<script src="<?php echo base_url();?>assets/select2/dist/js/select2.min.js"></script>

<!--

<script src="../resource/select2/jquery-3.2.1.min.js"></script>
 -->

<!-- Bootstrap core JavaScript -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="<?php echo base_url();?>assets/js/ie10-viewport-bug-workaround.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-dropdownhover.min.js"></script>

<script>

    $('#searchStudent').on('change', function(){
        if ($(this).is(':checked')) {
            $('form').attr('action', 'view.php');
        } else {
            $('form').attr('action', 'statement.php');
        }
    });
</script>

<script>
    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>


<!--datatables-->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
    } );
</script>

<!--datatables-->
</body>
</html>

