<body id="body">
<div class="container headSection">
    <div class="row">
        <div class="row">
            <div class="col-md-9">
                <a href="<?php echo base_url();?>" class="tittle"><h3>Expense & Income Management System.</h3></a>
            </div>
            <div class="col-md-3 text-right tittle">
                <p>Hi ! <b>Welcome User </b> &nbsp; <a class="tittle" href="<?php echo site_url();?>login/logout">[ Log Out ]</a></p>
                <p id='demo4' class="text-center cal-container"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-9 col-md-3 text-right searchbox">
                <!-- required for search, block 4 of 5 start -->
                <form class="form-group" id="searchForm" action="statement.php"  method="get" style="margin-top: 5px;  ">
                    <input class="form-control" type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
                    <input  type="checkbox" id="searchStudent"  name="byStudent" >Student
                    <input  type="checkbox"  name="byRemarks" checked  >By Remarks
                    <input  type="checkbox"  name="byHead"  >By Head
                    <input hidden type="submit" class="btn-primary" value="search">
                </form>
                <!-- required for search, block 4 of 5 end -->
            </div>
        </div>
        <nav  class="navbar navbar-default navbar-static">
            <div style="padding-left:0px; padding-right:0px;" class="container">
                <div style="padding-top:0px; margin:0px;" class="">
                </div>
                <div style="padding-top:0px; margin-top:0px;" class="navbar-header">
                    <button  type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!--<a class="navbar-brand" href="#myPage"><h4>BSBL</h4></a>-->
                </div>
                <div style="padding-top:0px; margin-top:0px;"  class="collapse navbar-collapse" id="myNavbar">
                    <div style="padding-left:0px; padding-right:0px;" class="container">
                        <ul class="nav navbar-nav navbar-left">
                            <li class="backg"><a href="<?php echo base_url('eims/index');?>">HOME</a></li>
                            <li class="dropdown">
                                <a id="menu" href="" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false" data-animations="zoomIn zoomIn zoomIn zoomIn">ACCOUNT INPUTS<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="backg"><a id="menu" href="<?php echo base_url('eims/addTransaction');?>">Single Transaction Entry</a></li>
                                    <li class="backg"><a id="menu" href="other/multipleVoucher.php">Multiple Voucher Entry</a></li>
                                    <li class="divider"></li>
                                    <li class="backg"><a id="menu" href="<?php echo base_url('eims/add');?>?item=student">Add Student</a></li>
                                    <li class="backg"><a id="menu" href="<?php echo base_url('eims/view/');?>?bookname=ALLSTUDENT">View Students</a></li>
                                    <li class="divider"></li>
                                    <li class="backg"><a id="menu" href="<?php echo base_url('eims/add');?>?item=head">Add Account Head</a></li>
                                    <li class="backg"><a id="menu" href="<?php echo base_url('eims/view/');?>?bookname=viewhead">View Head</a></li>
                                    <li class="backg"><a id="menu" href="create.php?id=addBank">Add Bank</a></li>
                                    <li class="backg"><a id="menu" href="addParty.php">Add Party</a></li>

                                </ul>
                            </li>

                            <li class="dropdown">
                                <a id="menu" href="" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false" data-animations="zoomIn zoomIn zoomIn zoomIn">ACOUNT REPORTS<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="backg"><a id="menu" href="<?php $mtype="currentMonth"; echo base_url('eims/statement/'.$mtype);?>">Cash Book</a></li>
                                    <li class="backg"><a id="menu" href="#">Bank Book</a></li>
                                    <li class="backg"><a id="menu" href="#">Ledger Book</a></li>
                                    <li class="backg"><a id="menu" href="<?php echo base_url('eims/trialbalance');?>?branchid=5&currentMonth=currentMonth&currentDate=<?php echo date('Y-m-01'); ?>"">Trial Balance</a></li>
                                    <li class="backg"><a id="menu" href="#">Trading Account</a></li>
                                    <li class="backg"><a id="menu" href="#">P/L Account</a></li>
                                    <li class="backg"><a id="menu" href="#">Statement of Affairs</a></li>
                                    <li class="backg"><a id="menu" href="opneningbalance.php">Opnening Balance</a></li>
                                    <li class="divider"></li>

                                </ul>
                            </li>
                            <li class="dropdown">
                                <a id="menu" href="" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false" data-animations="zoomIn zoomIn zoomIn zoomIn">SETUP<span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown">
                                    <li class="backg"><a id="menu" href="<?php $mtype="currentMonth"; echo base_url('eims/statement/'.$mtype);?>">General Settings</a></li>
                                    <li class="backg"><a id="menu" href="#">Staff Management</a>
                                        <ul class="dropdown-menu">
                                            <li class="backg"> <a id="menu" href="#">Admin Users</a></li>
                                        </ul>
                                    </li>
                                    <li class="backg"><a id="menu" href="#">Payment</a></li>
                                    <li class="backg"><a id="menu" href="trialBalance.php?branchid=5&currentMonth=currentMonth&currentDate=<?php echo date('Y-m-01'); ?>"">Product/Services</a></li>
                                    <li class="backg"><a id="menu" href="#">Support</a></li>
                                    <li class="backg"><a id="menu" href="#">Email Template</a></li>
                                    <li class="backg"><a id="menu" href="#">Notifications</a></li>
                                    <li class="backg"><a id="menu" href="opneningbalance.php">Other</a></li>
                                    <li class="divider"></li>

                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<!-- Page should be corrected -->