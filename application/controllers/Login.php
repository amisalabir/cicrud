<?php
class Login extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('login_model');
    }

    function index(){
        $data['page']=__FUNCTION__;
        if($this->session->userdata('logged_in') !== TRUE){
            $this->load->view('includes/header',$data);
            $this->load->view('User/Profile/signup');
            $this->load->view('includes/footer');
            $this->load->view('includes/footer_script');
        }else{
            redirect('eims');
        }

    }

    function auth(){
        $email    = $this->input->post('email',TRUE);
        $password = md5($this->input->post('password',TRUE));
        $validate = $this->login_model->validate($email,$password);
        if($validate->num_rows() > 0){
            $data  = $validate->row_array();
            $name  = $data['username'];
            $adminid  = $data['user_id'];
            $email = $data['user_email'];
            //$level = $data['user_level'];
            $roleid = $data['role'];
            $sesdata = array(
                'username'  => $name,
                'email'     => $email,
                'adminid'     => $adminid,
                'role'     => $roleid,
                'logged_in' => TRUE
            );

            $this->session->set_userdata($sesdata);
            redirect('Eims');
            /*

            // access login for admin
            if($roleid === '1'){
                redirect('eims');

                // access login for staff
            }elseif($roleid === '2'){
                redirect('page/staff');

                // access login for author
            }else{
                redirect('page/author');
            }
            */

        }else{

            echo $this->session->set_flashdata('msg','Username or Password is Wrong');
            redirect('login');
        }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }


}