<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Eims extends CI_Controller
{
    public $objPrivilege;

    public function __construct()
    {
        //load database in autoload libraries 
        parent::__construct();
        if ($this->session->userdata('logged_in') !== TRUE) {
            redirect('login');
        }
        $this->load->model('Eimsmodel');
        $this->load->model('Role');
        $this->load->model('PrivilegedUser');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $PrivilegedUser = new $this->PrivilegedUser();
        $this->objPrivilege = $PrivilegedUser->getByUsername($this->session->userdata('username'));
    }

    public function index()
    {

        $data['page']=__FUNCTION__;
        $mtype = "currentMonth";
        $data['branches'] = $this->Eimsmodel->getBranch();
        $data['book'] = $this->Eimsmodel->getBook();
        $data['data'] = $this->Eimsmodel->get_statement($mtype);
        $this->load->view('eims/index',$data);

    }
    public function trialbalance(){
        $mtype = "currentMonth";
        $data['data'] = $this->Eimsmodel->get_statement($mtype);
        $this->load->view('eims/index',$data);
    }
    public function tables(){

        $this->load->view('dashboard/tables');
    }
    public function datatables(){

        $this->load->view('dashboard/datatables');
    }

    public function addTransaction()
    {
        $data['page']=__FUNCTION__;
        if ($this->objPrivilege->hasPrivilege(__FUNCTION__)) {
            $data['branches'] = $this->Eimsmodel->getBranch();
            $data['accountHead'] = $this->Eimsmodel->getHead();
            $this->load->view('eims/addTransaction', $data);
        } else {
            $this->session->set_flashdata('msg', '<a class="text-danger">Access Denied !</a>');
            unset($_SESSION['msg']);
        }
    }

    public function add()
    {
        $data['page']=__FUNCTION__;
        $data['item'] = $_GET['item'];
        if ($this->objPrivilege->hasPrivilege(__FUNCTION__)) {
            $data['class'] = $this->Eimsmodel->getClass();
            $data['batch'] = $this->Eimsmodel->getBatch();
            $data['course'] = $this->Eimsmodel->getCourse();
            $this->load->view('includes/header',$data);
            $this->load->view('includes/nav');

            $this->load->view('eims/form');
            $this->load->view('includes/footer');
            $this->load->view('includes/footer_script');
            $this->load->view('includes/footer_script_1');
            $this->load->view('includes/footer_script_2');
        } else {
            $this->session->set_flashdata('msg', '<a class="text-danger">Access Denied !</a>');
            $this->load->view('includes/header');
            $this->load->view('includes/nav');
            $this->load->view('includes/footer');
            $this->load->view('includes/footer_script');
            $this->load->view('includes/footer_script_1');
            $this->load->view('includes/footer_script_2');
            unset($_SESSION['msg']);
        }
    }

    public function store()
    {


        $success = '';
        if (isset($_POST['addTransaction']) && $this->Eimsmodel->insert_trasnsaction_record($_POST)) {
            $success = true;
        }
        if (isset($_POST['addStudent']) && $this->Eimsmodel->insertNewStudent($_POST)) {
            $success = true;
        }

        if ($success) {
            $this->session->set_flashdata('msg', '<a class="text-success"> Your record has been inserted successfully !</a>');
            redirect(base_url());
            unset($_SESSION['msg']);
        }

    }

    public function statement($mtype)
    {
        $data['page']=__FUNCTION__;
########################## Previous & Next Data Calculation ##############################
        $prevDate = date('Y-m-01');
        $nextDate = date('Y-m-01');
        if (isset($_GET['monthid'])) {
            if ($_GET['monthid'] == 'prevtMonth') {
                $dt = $_GET['prevDate'];
                $date = strtotime(date("Y-m-d", strtotime($dt)) . ", first day of this month");
                $newdate = strtotime('-1 MONTH', $date);
                $newdate = date('Y-m-d', $newdate);
                $prev_last_date_find = strtotime(date("Y-m-d", strtotime($newdate)) . ", last day of this month");
                $prev_last_date = date('Y-m-d', $prev_last_date_find);
                $prevDate = $newdate;
                $_GET['fromTransaction'] = $prevDate;
                $_GET['toTransaction'] = $prev_last_date;
                $nextDate = $newdate;
                //echo "</br>".$newdate;
                //echo "</br>".$prev_last_date;
                $allData = $objTransaction->setData($_GET);
            }
            if ($_GET['monthid'] == 'nextMonth') {
                $ndt = $_GET['nextDate'];
                $NewDate = strtotime(date("Y-m-d", strtotime($ndt)) . ", first day of this month");
                $nextDate = strtotime('+1 MONTH', $NewDate);
                $nextNewDate = date('Y-m-d', $nextDate);
                $next_last_date_find = strtotime(date("Y-m-d", strtotime($nextNewDate)) . ", last day of this month");
                $next_last_date = date('Y-m-d', $next_last_date_find);
                $nextDate = $nextNewDate;
                $_GET['fromTransaction'] = $nextNewDate;
                $_GET['toTransaction'] = $next_last_date;
                $prevDate = $nextNewDate;
                $nextDate = $nextNewDate;
                //echo "</br>".$nextNewDate;
                // echo "</br>".$next_last_date;
                $allData = $objTransaction->setData($_GET);
            }
            $fromTransaction = $_GET['fromTransaction'];
            $toTransaction = $_GET['toTransaction'];
            $branch = "(Personal)";
        }
###################################### Previous & Next Data Calculation End ##############################

        if ($this->objPrivilege->hasPrivilege('read')) {
            $data['data'] = $this->Eimsmodel->get_statement($mtype);
            $this->load->view('includes/header', $data);
            $this->load->view('includes/nav');
            $this->load->view('eims/statement');
            $this->load->view('includes/footer');
            $this->load->view('includes/footer_script');
            $this->load->view('includes/footer_script_1');
            $this->load->view('includes/footer_script_2');

        }

    }

    public function view()
    {

        if ($this->objPrivilege->hasPrivilege('read')) {
            $data['bookname'] = $_GET['bookname'];
            $data['data'] = $this->Eimsmodel->getView($_GET);
            $this->load->view('includes/header');
            $this->load->view('includes/nav');
            $this->load->view('eims/view', $data);
            $this->load->view('includes/footer');
            $this->load->view('includes/footer_script');
            $this->load->view('includes/footer_script_1');
            $this->load->view('includes/footer_script_2');

        }

    }

    function staff()
    {
        //Allowing akses to staff only
        if ($this->session->userdata('level') === '2') {
            $this->load->view('dashboard_view');
        } else {
            echo "Access Denied";
        }
    }

    function author()
    {
        //Allowing akses to author only
        if ($this->session->userdata('level') === '3') {
            $this->load->view('dashboard_view');
        } else {
            echo "Access Denied";
        }
    }

    public function create()
    {
        $this->load->view('includes/header');
        $this->load->view('includes/nav');
        $this->load->view('crud/create');
        $this->load->view('includes/footer');
        $this->load->view('includes/footer_script');
    }

    public function edit($id)
    {
        $product = $this->db->get_where('products', array('id' => $id))->row();
        $this->load->view('includes/header');
        $this->load->view('includes/nav');
        $this->load->view('crud/edit', array('product' => $product));
        $this->load->view('includes/footer');
    }

    public function update($id)
    {
        $data['data'] = $this->Crudmodel->update_product($id);
        redirect(base_url());
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('products');

        redirect(base_url());
    }
}

