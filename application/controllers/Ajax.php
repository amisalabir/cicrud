<?php
class Ajax extends CI_Controller{
    private $singleArray = array();

    public function __construct() {
        //load database in autoload libraries
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        }
        $this->load->model('Ajaxmodel');

    }
    public function objectToarray($object){
        return $myArray = json_decode(json_encode($object), true);
    }

    public function multiArrayToSingle($multiDimensionalArray){
        //$singleArray = array();

        foreach ($multiDimensionalArray as $key => $value){
            $this->singleArray[$key] = $value;
        }
        return $this->singleArray[$key];

    }

    public function index(){
        redirect(base_url());
    }

    public function listData(){
        $data['data']=$this->Ajaxmodel->getListData();
        echo json_encode(array('data'=>$data['data']));
    }

}